package it.com.atlassian.confluence.webdriver.pageobjects;

import com.atlassian.confluence.api.model.people.User;
import com.atlassian.confluence.webdriver.pageobjects.component.ConfluenceAbstractPageComponent;
import com.atlassian.confluence.webdriver.pageobjects.page.content.ViewPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

public class SharelinksWizard extends ConfluenceAbstractPageComponent {
    @ElementBy(id = "sharelinks-url")
    private PageElement urlField;

    @ElementBy(id = "sharelinks-title")
    private PageElement titleField;

    @ElementBy(id = "sharelinks-comment")
    private PageElement commentField;

    @ElementBy(xpath = "(//*[contains(concat(' ', @class, ' '), 'create-dialog-create-button')])[last()]")
    private PageElement submit;

    @ElementBy(cssSelector = ".sharelinks-urlmacro-button")
    private PageElement bookmarklet;

    @ElementBy(cssSelector = ".sharelinks-preview")
    private PageElement linkPreview;

    private final String TITLE_SELECTOR = "#create-dialog h3.sharelinks-preview-title";

    public ViewPage submit() {
        submit.click();
        waitUntilFalse("Submit element should be visible", submit.timed().isVisible());
        return pageBinder.bind(ViewPage.class);
    }

    public SharelinksWizard clickSubmit() {
        submit.click();
        return this;
    }

    public SharelinksWizard setUrl(String url) {
        urlField.clear().type(url);
        //manually trigger change event since webdriver sendkeys only sometimes fires onchange event, even when elem is unfocused.
        urlField.javascript().execute("jQuery(arguments[0]).trigger(\"change\")");
        return this;
    }

    public SharelinksWizard setTitle(String title) {
        waitUntilTitleFieldIsEnabled();
        titleField.clear().type(title);
        return this;
    }

    public String getTitle() {
        return titleField.getValue();
    }

    public SharelinksWizard setComment(String comment) {
        waitUntilTrue("Comment field should be enabled", commentField.timed().isEnabled());
        commentField.clear().type(comment);
        return this;
    }

    public SharelinksWizard waitForCommentFieldToBeDisabled() {
        waitUntilFalse("Comment field should not be enabled", commentField.timed().isEnabled());
        return this;
    }


    public String getURLValidationError() {
        WebElement errorDiv = driver.findElement(By.cssSelector("#sharelinks-url + .error"));
        return errorDiv.getText();
    }

    public String getTitleValidationError() {
        WebElement errorDiv = driver.findElement(By.cssSelector("#sharelinks-title + .error"));
        return errorDiv.getText();
    }

    public void assertWizardIsShown() {
        waitUntilTrue("URL field is not visible", urlField.timed().isVisible());
    }

    public String getPreviewTitle() {
        waitForPreviewLoaded();
        WebElement previewTitle = driver.findElement(By.cssSelector(TITLE_SELECTOR));
        return previewTitle.getText();
    }

    public SharelinksWizard validateLinkPreviewRendered() {
        waitForPreviewLoaded();
        waitUntilTrue("Link preview should be present", linkPreview.timed().isPresent());
        return this;
    }

    public SharelinksWizard waitForPreviewLoaded() {
        waitUntilTrue("Preview should be visible", pageElementFinder.find(By.cssSelector(TITLE_SELECTOR)).timed().isVisible());
        return this;
    }

    private SharelinksWizard waitUntilTitleFieldIsEnabled() {
        waitUntilTrue("Title field should be enabled", titleField.timed().isEnabled());
        return this;
    }

    public void pasteUrl(String pasteValue) {
        // paste event is not triggered reliably when actually pasting in a url,
        // mimic user pasting instead by typing in the value and manually fire
        // a paste event.
        urlField.type(pasteValue);
        urlField.javascript().execute("jQuery(arguments[0]).trigger(\"paste\")");
    }

    public SharelinksWizard addUserToShareWith(User user) {
        Picker userPicker = pageBinder.bind(Picker.class);
        userPicker.bindingElements("sharelinks-sharewith");
        userPicker.search(user.getUsername()).selectUser(user.getUsername());
        return this;
    }

    public LabelPicker inputLabel(String labelName) {
        LabelPicker labelPicker = getLabelPicker();
        labelPicker.inputLabel(labelName);
        return labelPicker;
    }

    public LabelPicker addLabel(String labelName) {
        LabelPicker labelPicker = inputLabel(labelName);
        labelPicker.selectLabel(labelName);
        return labelPicker;
    }

    public LabelPicker getLabelPicker() {
        return pageBinder.bind(LabelPicker.class);
    }

    public SharelinksWizard validateBookmarkletPresent() {
        waitUntilTrue("Bookmarklet is not present", bookmarklet.timed().isPresent());
        return this;
    }
}