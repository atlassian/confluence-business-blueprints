package it.com.atlassian.confluence.webdriver;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.ContentStatus;
import com.atlassian.confluence.api.service.content.ContentService;
import com.atlassian.confluence.rest.api.model.ExpansionsParser;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.ResetFixtures;
import com.atlassian.confluence.test.stateless.rules.WebSudoRule;
import com.atlassian.confluence.webdriver.pageobjects.component.blueprint.BlueprintDialog;
import com.atlassian.confluence.webdriver.pageobjects.component.blueprint.HowTo;
import com.atlassian.confluence.webdriver.pageobjects.page.DashboardPage;
import com.atlassian.confluence.webdriver.pageobjects.page.admin.ConfluenceAdminHomePage;
import com.atlassian.confluence.webdriver.pageobjects.page.content.CreatePage;
import it.com.atlassian.confluence.webdriver.pageobjects.MeetingNotesViewPage;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests the "Meeting Notes" Blueprint.
 */
@RunWith(ConfluenceStatelessTestRunner.class)
public class MeetingNotesBlueprintTest extends AbstractBlueprintTest {
    private static final String PLUGIN_KEY = "com.atlassian.confluence.plugins.confluence-business-blueprints";
    private static final String CREATE_DIALOG_MODULE_KEY = PLUGIN_KEY + ":meeting-notes-item";
    private static final String INDEX_PAGE_TITLE = "Meeting notes";

    @Rule
    public WebSudoRule webSudoRule = new WebSudoRule(false);

    @Override
    public void setUp() throws Exception {
        super.setUp();

        // Need to set the locale explicitly because this test checks date formats
        rpcClient.getAdminSession().getFunctestComponent().setUserLocale(user.get(), new Locale("en", "AU"));
    }

    @Test
    @ResetFixtures({"space"})
    public void newMeetingNotesPageAppearOnIndexPage() {
        final HowTo howTo = openHowToWizardFromDashboard();
        final CreatePage editor = howTo.clickNext(CreatePage.class);

        final String expectedPageTitle = defaultMeetingNotesTitleForToday();
        assertThat(editor.getTitle(), is(expectedPageTitle));

        saveEditorAndCheckIndexPageContent(editor, space.get(), INDEX_PAGE_TITLE);
    }

    // CONFDEV-15858
    @Test
    @ResetFixtures({"space"})
    public void createMeetingNotesFromAdminPage() {
        product.login(admin.get(), ConfluenceAdminHomePage.class);
        HowTo howTo = openHowToWizardFromCurrentPage();
        CreatePage editor = howTo.clickNext(CreatePage.class);

        final String expectedPageTitle = defaultMeetingNotesTitleForToday();
        assertThat(editor.getTitle(), is(expectedPageTitle));

        saveEditorAndCheckIndexPageContent(editor, space.get(), INDEX_PAGE_TITLE);
    }

    // CONFDEV-15795
    @Test
    @ResetFixtures({"space"})
    public void loggedInUserAppearsAsFirstAttendee() {
        HowTo howTo = openHowToWizardFromDashboard();
        howTo.clickNext(CreatePage.class).setTitle(defaultMeetingNotesTitleForToday()).save();
        MeetingNotesViewPage meetingNotesPage = product.getPageBinder().bind(MeetingNotesViewPage.class);

        assertEquals(user.get().getDisplayName(), meetingNotesPage.getFirstAttendee());
    }

    // CONFDEV-15795
    @Test
    @Ignore("TODO: Enable test when CONFDEV-33959 is fixed")
    public void anonymousUserDoesNotAppearsAsFirstAttendee() {
        grantAnonymousPermissions();

        product.logOut();

        DashboardPage dashboardPage = product.visit(DashboardPage.class);
        assertFalse(dashboardPage.getHeader().isLoggedIn());

        HowTo howTo = openHowToWizardFromDashboard();
        howTo.clickNext(CreatePage.class).setTitle(defaultMeetingNotesTitleForToday()).save();
        MeetingNotesViewPage meetingNotesPage = product.getPageBinder().bind(MeetingNotesViewPage.class);

        assertThat(meetingNotesPage.getFirstAttendee(), isEmptyString());
    }

    //CONFDEV-23720
    @Test
    @ResetFixtures({"space"})
    public void newMeetingNotesHasDateLoz() {
        final HowTo howTo = openHowToWizardFromDashboard();
        final CreatePage editor = howTo.clickNext(CreatePage.class);
        assertTrue(editor.hasHtmlContent("</time>"));
        assertTrue(editor.hasHtmlContent("datetime=\"" + getTodayStr() + "\""));
    }

    @Test
    @ResetFixtures({"space"})
    public void blueprintTemplateUsingCorrectMentionStorageFormat() {
        final HowTo howTo = openHowToWizardFromDashboard();
        final CreatePage editor = howTo.clickNext(CreatePage.class);

        Content meetingNotesDraft = restClient.getAdminSession().contentService()
                .find(ExpansionsParser.parse(ContentService.DEFAULT_EXPANSIONS))
                .withSpace(space.get())
                .withStatus(ContentStatus.DRAFT)
                .fetchOrNull();

        assertThat(meetingNotesDraft.getBody().get(ContentRepresentation.STORAGE).getValue(),
                not(containsString("<ri:user ri:username=")));
        assertThat(meetingNotesDraft.getBody().get(ContentRepresentation.STORAGE).getValue(),
                containsString("<ri:user ri:userkey="));
    }

    private String defaultMeetingNotesTitleForToday() {
        return getTodayStr() + " Meeting notes";
    }

    private String getTodayStr() {
        final Locale locale = rpcClient.getAdminSession().getFunctestComponent().getUserLocale(user.get());
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", locale);
        return dateFormat.format(new Date());
    }

    private HowTo openHowToWizardFromDashboard() {
        return openHowToWizard(openCreateDialog());
    }

    private HowTo openHowToWizardFromCurrentPage() {
        return openHowToWizard(openCreateDialogOnCurrentPage());
    }

    private HowTo openHowToWizard(BlueprintDialog blueprintDialog) {
        blueprintDialog.openSpaceSelect().selectSpace(space.get());
        return selectBlueprint(blueprintDialog, HowTo.class, CREATE_DIALOG_MODULE_KEY);
    }
}
