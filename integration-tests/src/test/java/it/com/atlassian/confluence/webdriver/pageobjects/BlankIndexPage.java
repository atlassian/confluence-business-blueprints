package it.com.atlassian.confluence.webdriver.pageobjects;

import com.atlassian.confluence.webdriver.pageobjects.component.ConfluenceAbstractPageComponent;
import org.openqa.selenium.By;

public class BlankIndexPage extends ConfluenceAbstractPageComponent {
    public <T> T clickOnCreateButtonAndExpectWizard(Class<T> clazz) {
        String buttonSelector = ".blueprint-blank-experience .create-from-template-button";
        pageElementFinder.find(By.cssSelector(buttonSelector)).click();
        return pageBinder.bind(clazz);
    }
}
