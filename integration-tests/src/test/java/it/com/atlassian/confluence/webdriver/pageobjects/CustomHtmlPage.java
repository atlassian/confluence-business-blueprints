package it.com.atlassian.confluence.webdriver.pageobjects;

import static com.atlassian.webdriver.utils.by.ByJquery.$;

import com.atlassian.confluence.webdriver.pageobjects.page.ConfluenceAbstractPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.utils.by.ByJquery;

public class CustomHtmlPage extends ConfluenceAbstractPage {
    // The button confirm for submit the Custom Html form
    @ElementBy(id = "confirm")
    private PageElement confirmCustomHtml;

    @ElementBy(id = "beforeHeadEnd")
    private PageElement beforeHeadEnd;

    @ElementBy(id = "afterBodyStart")
    private PageElement afterBodyStart;

    @ElementBy(id = "beforeBodyEnd")
    private PageElement beforeBodyEnd;

    /**
     * Using Confluence Admin - Custom HTML - Input custom html into At end of the head textarea
     *
     * @param customHeadHtml the custom head html
     * @return CustomHtmlWizard
     */
    public CustomHtmlPage setCustomHeadHtml(String customHeadHtml) {
        // find At the end of the head textarea for input custom head html
        ByJquery jQuerySelector = $(":input[name='beforeHeadEnd']");
        PageElement endHeadTextArea = pageElementFinder.find(jQuerySelector);
        endHeadTextArea.clear().type(customHeadHtml);
        return this;
    }

    public CustomHtmlPage clearCustomHtml() {
        beforeHeadEnd.clear();
        afterBodyStart.clear();
        beforeBodyEnd.clear();

        return this;
    }

    public CustomHtmlPage clickConfirmCustomHtml() {
        confirmCustomHtml.click();
        return this;
    }

    @Override
    public String getUrl() {
        return "/admin/editcustomhtml.action";
    }
}
