package com.atlassian.confluence.plugins.sharelinks;

import com.atlassian.confluence.content.render.image.ImageDimensions;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.DefaultImagePlaceholder;
import com.atlassian.confluence.macro.EditorImagePlaceholder;
import com.atlassian.confluence.macro.ImagePlaceholder;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.plugins.BusinessBlueprintsContextProviderHelper;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.google.common.collect.Maps;

import java.util.HashMap;
import java.util.Map;

/**
 * @since 2.0
 */
public class URLMacro implements Macro, EditorImagePlaceholder {
    private final SettingsManager settingsManager;
    private final BusinessBlueprintsContextProviderHelper helper;
    private static final String IMAGE_PATH = "/download/resources/com.atlassian.confluence.plugins.confluence-business-blueprints:sharelinks-urlmacro-editor-resources/sharelinks-urlmacro-placeholder.png";

    public URLMacro(SettingsManager settingsManager, BusinessBlueprintsContextProviderHelper helper) {
        this.settingsManager = settingsManager;
        this.helper = helper;
    }

    @Override
    public String execute(Map<String, String> parameters, String bodyText, ConversionContext conversionContext) throws MacroExecutionException {
        String soyBookmarkletLinkTemplateName = "Confluence.Blueprints.SharelinksUrlMacro.bookmarkletLink.soy";

        String bookmarkletActionURL = settingsManager.getGlobalSettings().getBaseUrl() + "/plugins/sharelinksbookmarklet/bookmarklet.action";
        HashMap<String, Object> soyLinkMetaDataContext = Maps.newHashMap();
        soyLinkMetaDataContext.put("bookmarkletActionURL", bookmarkletActionURL);

        return helper.renderFromSoy(
                "com.atlassian.confluence.plugins.confluence-business-blueprints:sharelinks-urlmacro-resources",
                soyBookmarkletLinkTemplateName, soyLinkMetaDataContext);
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

    @Override
    public ImagePlaceholder getImagePlaceholder(Map<String, String> params, ConversionContext ctx) {
        return new DefaultImagePlaceholder(IMAGE_PATH, false, new ImageDimensions(175, 30));
    }

}
