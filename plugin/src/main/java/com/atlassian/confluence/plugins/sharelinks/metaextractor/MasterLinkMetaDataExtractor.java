package com.atlassian.confluence.plugins.sharelinks.metaextractor;

import com.atlassian.confluence.plugins.sharelinks.DOMMetadataExtractor;
import com.atlassian.confluence.plugins.sharelinks.LinkMetaData;
import com.atlassian.confluence.plugins.sharelinks.LinkMetaDataExtractor;
import com.atlassian.plugins.whitelist.NotAuthorizedException;
import com.atlassian.plugins.whitelist.OutboundWhitelist;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.collect.ImmutableList;
import org.apache.hc.core5.http.HeaderElement;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.http.message.BasicHeaderValueParser;
import org.apache.hc.core5.http.message.HeaderValueParser;
import org.apache.hc.core5.http.message.ParserCursor;
import org.apache.http.entity.ContentType;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.regex.Pattern;

import static com.atlassian.sal.api.net.Request.MethodType.GET;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.DOTALL;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * @since 1.7.2
 */
@Component
public class MasterLinkMetaDataExtractor implements LinkMetaDataExtractor {
    public static final String CSSQUERY_CONTENT_TYPE = "meta[http-equiv=Content-Type][content]";
    public static final String CONTENT = "content";
    public static final String CHARSET = "charset";
    private static final Pattern UNTIL_END_HEAD_OR_EOF_PATTERN = Pattern.compile(".*?</head>|.*", CASE_INSENSITIVE | DOTALL);
    private static final int MAX_HEAD_SIZE = 128 * 1024; // 128K chars ought to be enough for anybody!
    private static final int DESCRIPTION_MAX_LENGTH = 180;
    private static final int DOMAIN_MAX_LENGTH = 50;
    private static final int EXCERPT_URL_MAX_LENGTH = 30;
    private final OutboundWhitelist outboundWhitelist;

    private static final Logger log = LoggerFactory.getLogger(MasterLinkMetaDataExtractor.class);

    private final List<DOMMetadataExtractor> metadataExtractors;
    private final RequestFactory<?> requestFactory;

    public MasterLinkMetaDataExtractor(final RequestFactory<?> requestFactory,
                                       final OutboundWhitelist outboundWhitelist) {
        this.requestFactory = requestFactory;
        this.outboundWhitelist = outboundWhitelist;
        metadataExtractors = ImmutableList.of(
                new OpenGraphDOMMetadataExtractor(),
                new TwitterDOMMetadataExtractor(),
                new SimpleDOMMetadataExtractor(requestFactory)
        );
    }

    @Override
    public LinkMetaData parseMetaData(String url, boolean isPreview) throws URISyntaxException, NotAuthorizedException {
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }

        LinkMetaData meta = new LinkMetaData(url);
        meta.setExcerptedURL(getExcerptedUrl(url));

        //invalid URIs return early
        URI uri = new URI(url);
        String domain = isBlank(uri.getHost()) ? url : uri.getHost();
        meta.setDomain(getPreviewText(domain, DOMAIN_MAX_LENGTH));

        String htmlData = getHeadHtmlData(url, meta);
        Document jsoupDoc = Jsoup.parse(htmlData);
        if (!htmlData.isEmpty()) {
            for (DOMMetadataExtractor metadataExtractor : metadataExtractors) {
                metadataExtractor.updateMetadata(meta, jsoupDoc);
            }
        }

        // cut the description for preview panel
        if (isPreview) {
            meta.setDescription(getPreviewText(meta.getDescription(), DESCRIPTION_MAX_LENGTH));
        }

        return meta;
    }

    /**
     * Extract everything from the start of the HTML document until the head tag is closed, or {@link #MAX_HEAD_SIZE}
     * is reached.
     *
     * @param url full url to a web page
     * @return A chunk of HTML which is the head of the document
     */
    @NonNull
    private String getHeadHtmlData(String url, LinkMetaData meta) throws NotAuthorizedException {
        if (!outboundWhitelist.isAllowed(URI.create(url))) {
            log.error("Not authorized to access this url. Please contact admin to add this url to whitelist.");
            throw new NotAuthorizedException(url);
        }

        final Request<?, ?> request = requestFactory.createRequest(GET, url);
        request.setHeader("accept-charset", "utf-8");

        try {
            return request.executeAndReturn(response -> processResponse(url, meta, response))
                    .orElse("{}");

        } catch (ResponseException ex) {
            log.error("Failed to make request", ex);
            return "{}";
        }
    }

    /**
     * @return the content of the HTML header
     */
    private Optional<String> processResponse(String url, LinkMetaData meta, Response response) throws ResponseException {
        meta.setResponseHost(URI.create(url));

        if (isValidResponse(response)) {
            try (InputStream inputStream = response.getResponseBodyAsStream()) {
                Charset charset = getContentType(response).getCharset();
                if (charset == null) {
                    inputStream.mark(Integer.MAX_VALUE);
                    charset = detectCharset(inputStream);
                    inputStream.reset();
                }

                meta.setCharset(charset == null ? UTF_8.name() : charset.name());
                return extractHtmlHeaderContent(inputStream, meta.getCharset());
            } catch (IOException ex) {
                throw new ResponseException(ex);
            }
        }
        return Optional.empty();
    }

    @NonNull
    private static ContentType getContentType(Response response) {
        return ContentType.parse(response.getHeader("Content-Type"));
    }

    @Nullable
    private static Charset detectCharset(InputStream inputStream) {
        final Scanner bodyScanner = new Scanner(inputStream, UTF_8.name());
        String attempt = bodyScanner.findWithinHorizon(UNTIL_END_HEAD_OR_EOF_PATTERN, MAX_HEAD_SIZE);
        Document jsoupDoc = Jsoup.parse(attempt);
        String contentType = jsoupDoc.select(CSSQUERY_CONTENT_TYPE).attr(CONTENT);
        HeaderValueParser parser = new BasicHeaderValueParser();
        ParserCursor cursor = new ParserCursor(0, contentType.length());
        HeaderElement[] contents = parser.parseElements(contentType, cursor);
        for (HeaderElement headerElement : contents) {
            NameValuePair charsetParam = headerElement.getParameterByName(CHARSET);
            if (charsetParam != null) {
                return Charset.forName(charsetParam.getValue());
            }
        }
        return null;
    }

    @NonNull
    private static Optional<String> extractHtmlHeaderContent(InputStream inputStream, String charset) {
        final Scanner responseScanner = new Scanner(inputStream, charset);
        return Optional.ofNullable(responseScanner.findWithinHorizon(UNTIL_END_HEAD_OR_EOF_PATTERN, MAX_HEAD_SIZE));
    }

    /**
     * Return the text that is cut at word boundaries and doesn't exceed max
     * length
     *
     * @return String
     */
    private static String getPreviewText(String text, int maxLength) {
        if (text == null || text.length() <= maxLength) {
            return text;
        }

        text = text.substring(0, maxLength);

        int lastSpaceIndex = text.lastIndexOf(' ');
        if (lastSpaceIndex != -1) {
            text = text.substring(0, lastSpaceIndex);
        }

        return text + '…';
    }

    private String getExcerptedUrl(String sourceUrl) {
        String excerptedUrl = sourceUrl;
        // Remove the http/https in the source url
        String split = "//";
        int splitIndex = excerptedUrl.indexOf(split);
        excerptedUrl = excerptedUrl.substring(splitIndex + split.length());
        if (excerptedUrl.length() > EXCERPT_URL_MAX_LENGTH) {
            excerptedUrl = excerptedUrl.substring(0, EXCERPT_URL_MAX_LENGTH - 1);
            excerptedUrl = excerptedUrl + '…';
        }
        return excerptedUrl;
    }

    /**
     * Ensures the response was successful and the content type is valid if set.
     *
     * @param response the HTTP response
     * @return true if the response is valid for DOM extraction, false otherwise.
     */
    private boolean isValidResponse(Response response) {
        final int statusCode = response.getStatusCode();
        final String mimeType = getContentType(response).getMimeType();
        return statusCode >= 200 && statusCode < 300 && (mimeType == null || mimeType.startsWith("text/"));
    }
}
