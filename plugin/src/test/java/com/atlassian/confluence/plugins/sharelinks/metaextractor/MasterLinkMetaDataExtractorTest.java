package com.atlassian.confluence.plugins.sharelinks.metaextractor;

import com.atlassian.confluence.plugins.sharelinks.LinkMetaData;
import com.atlassian.plugins.whitelist.NotAuthorizedException;
import com.atlassian.plugins.whitelist.OutboundWhitelist;
import com.atlassian.sal.core.net.HttpClientRequestFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockserver.junit.MockServerRule;
import org.mockserver.model.MediaType;

import java.net.URI;

import static com.atlassian.confluence.plugins.sharelinks.metaextractor.SimpleDOMMetadataExtractor.getFaviconUri;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.when;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.mockserver.model.MediaType.APPLICATION_OCTET_STREAM;
import static org.mockserver.model.MediaType.TEXT_HTML;

@RunWith(MockitoJUnitRunner.class)
public class MasterLinkMetaDataExtractorTest {

    @Rule
    public MockServerRule mockServerRule = new MockServerRule(this);

    private URI uri;
    private MasterLinkMetaDataExtractor masterLinkMetaDataExtractor;

    @Mock
    private OutboundWhitelist outboundWhitelist;

    @Before
    public void setUp() {
        uri = URI.create(String.format("http://localhost:%d/confluence/blueprints/business/sharedlinks.html", mockServerRule.getPort()));

        when(outboundWhitelist.isAllowed(uri)).thenReturn(true);

        masterLinkMetaDataExtractor = new MasterLinkMetaDataExtractor(new HttpClientRequestFactory(), outboundWhitelist);
    }

    @Test
    public void testTitleExtraction() throws Exception {
        setupResponses(200, TEXT_HTML.withCharset(UTF_8), "<html><head><title>My Awesome Title</title></head><body>");

        final LinkMetaData metaData = masterLinkMetaDataExtractor.parseMetaData(uri.toString(), false);
        assertEquals("My Awesome Title", metaData.getTitle());
    }

    @Test
    public void testTitleExtractionWithUpperCaseHtml() throws Exception {
        setupResponses(200, TEXT_HTML.withCharset(UTF_8), "<HTML><HEAD><TITLE>My Awesome Title</TITLE></HEAD><BODY>");

        final LinkMetaData metaData = masterLinkMetaDataExtractor.parseMetaData(uri.toString(), false);
        assertEquals("My Awesome Title", metaData.getTitle());
    }

    @Test
    public void testNon200Code() throws Exception {
        setupResponses(203, TEXT_HTML.withCharset(UTF_8),"<html><head><title>My Awesome Title</title></head><body>");

        final LinkMetaData metaData = masterLinkMetaDataExtractor.parseMetaData(uri.toString(), false);
        assertEquals("My Awesome Title", metaData.getTitle());
    }

    @Test
    public void testTitleExtractionWithNoEndHead() throws Exception {
        setupResponses(200, TEXT_HTML.withCharset(UTF_8), "<html><head><title>My Awesome Title</title><body></body></html>");

        final LinkMetaData metaData = masterLinkMetaDataExtractor.parseMetaData(uri.toString(), false);
        assertEquals("My Awesome Title", metaData.getTitle());
    }

    @Test
    public void testTitleExtractionWithNewLines() throws Exception {
        setupResponses(200, TEXT_HTML.withCharset(UTF_8), "<html>\n<head>\n<title>My Awesome Title</title>\n</head>\n<body>");

        final LinkMetaData metaData = masterLinkMetaDataExtractor.parseMetaData(uri.toString(), false);
        assertEquals("My Awesome Title", metaData.getTitle());
    }

    @Test
    public void testTitleOutsideHeadIgnored() throws Exception {
        setupResponses(200, TEXT_HTML.withCharset(UTF_8), "<html>\n<head></head>\n<title>My Awesome Title</title>\n<body>");

        final LinkMetaData metaData = masterLinkMetaDataExtractor.parseMetaData(uri.toString(), false);
        assertThat("My Awesome Title", not(metaData.getTitle()));
    }

    @Test
    public void testNonTextContentTypeIgnored() throws Exception {
        setupResponses(200, APPLICATION_OCTET_STREAM.withCharset(UTF_8), "<html>\n<head></head>\n<title>My Awesome Title</title>\n<body>");

        final LinkMetaData metaData = masterLinkMetaDataExtractor.parseMetaData(uri.toString(), false);
        assertThat("My Awesome Title", not(metaData.getTitle()));
    }

    @Test
    public void testGetHeadHtmlWhenCharsetIsNull() throws Exception {
        setupResponses(200, TEXT_HTML, "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=Shift_JIS\"><title>My Awesome Title</title></head><body>");

        LinkMetaData metaData = masterLinkMetaDataExtractor.parseMetaData(uri.toString(), false);
        assertEquals("Shift_JIS", metaData.getCharset());
    }
    @Test
    public void testCharsetDefaultsToUTF8() throws Exception {
        setupResponses(200, TEXT_HTML, "<html><head><title>My Awesome Title</title></head><body>");

        LinkMetaData metaData = masterLinkMetaDataExtractor.parseMetaData(uri.toString(), false);
        assertEquals("UTF-8", metaData.getCharset());
    }

    @Test
    public void testInvalidMetaContentTypeTag() throws Exception {
        setupResponses(200, TEXT_HTML, "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; otherStuff; charset=Shift_JIS\"><title>My Awesome Title</title></head><body>");

        LinkMetaData metaData = masterLinkMetaDataExtractor.parseMetaData(uri.toString(), false);
        assertEquals("Shift_JIS", metaData.getCharset());
    }

    @Test
    public void testNonWhitelistHostThrowsNotAuthorizedException() throws Exception {
        when(outboundWhitelist.isAllowed(uri)).thenReturn(false);

        assertThrows(NotAuthorizedException.class, () ->
                masterLinkMetaDataExtractor.parseMetaData(uri.toString(), false));
    }

    private void setupResponses(int responseCode, MediaType mediaType, String responseText) {
        mockServerRule.getClient().when(request(uri.getPath()))
                .respond(response()
                        .withStatusCode(responseCode)
                        .withContentType(mediaType)
                        .withBody(responseText)
                );

        mockServerRule.getClient().when(request(getFaviconUri(uri).getPath()))
                .respond(response());
    }
}
